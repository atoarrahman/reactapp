import React, { Component } from 'react';

export default class Input extends Component {
    state = {
        title: '',
        placeholder: 'Enter title',
    };

    handleChange = (e) => {
        this.setState({
            title: e.target.value,
        });
    };

    render() {
        const { title, placeholder } = this.state;
        return (
            <div>
                <form>
                    <p>Reasult: {title}</p>
                    <input type="text" placeholder={placeholder} onChange={this.handleChange} />
                </form>
            </div>
        );
    }
}
