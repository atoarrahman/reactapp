import React, { Component } from 'react';

export default class Textarea extends Component {
    state = {
        text: '',
        placeholder: 'Enter Your Text',
    };

    handleChange = (e) => {
        if (e.target.name === 'textareaField') {
            this.setState({
                text: e.target.value,
            });
        }
    };

    render() {
        const { text, placeholder } = this.state;
        return (
            <form>
                <p>Reasult: {text}</p>
                <textarea
                    type="textarea"
                    name="textareaField"
                    placeholder={placeholder}
                    value={text}
                    onChange={this.handleChange}
                />
            </form>
        );
    }
}
