import React, { Component } from 'react';

export default class Select extends Component {
    state = {
        library: 'React',
    };

    handleChange = (e) => {
        // if (e.target.type === 'select-one') {
        //     this.setState({
        //         library: e.target.value,
        //     });
        // }

        if (e.target.name === 'selectField') {
            this.setState({
                library: e.target.value,
            });
        }
    };

    render() {
        const { library } = this.state;
        return (
            <div>
                <form>
                    <p>Reasult: {library}</p>
                    <select name="selectField" value={library} onChange={this.handleChange}>
                        <option value="Svelte">Svelte</option>
                        <option value="React">React</option>
                        <option value="JQuary">JQuary</option>
                    </select>
                </form>
            </div>
        );
    }
}
