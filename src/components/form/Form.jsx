import React from 'react';

export default class Form extends React.Component {
    state = {
        title: 'Welcome!!',
        text: 'React is awesome',
        library: 'React',
        isAwesome: false,
    };

    handleChange = (e) => {
        if (e.target.name === 'inputField') {
            this.setState({
                title: e.target.value,
            });
        } else if (e.target.name === 'textareaField') {
            this.setState({
                text: e.target.value,
            });
        } else if (e.target.name === 'selectField') {
            this.setState({
                library: e.target.value,
            });
        } else if (e.target.name === 'checkboxField') {
            this.setState({
                isAwesome: e.target.checked,
            });
        } else {
            console.log('nothing here');
        }
    };

    submitHandler = (e) => {
        const { title, text, library, isAwesome } = this.state;
        e.preventDefault();

        console.log(title, text, library, isAwesome);
    };

    render() {
        const { title, text, library, isAwesome } = this.state;
        return (
            <div>
                <form onSubmit={this.submitHandler}>
                    <input
                        type="text"
                        name="inputField"
                        value={title}
                        placeholder=""
                        onChange={this.handleChange}
                    />
                    <br />
                    <br />
                    <textarea
                        type="textarea"
                        name="textareaField"
                        placeholder=""
                        value={text}
                        onChange={this.handleChange}
                    />
                    <br />
                    <br />
                    <select name="selectField" value={library} onChange={this.handleChange}>
                        <option value="Svelte">Svelte</option>
                        <option value="React">React</option>
                        <option value="JQuary">JQuary</option>
                    </select>
                    <br />
                    <br />
                    <input
                        type="checkbox"
                        name="checkboxField"
                        checked={isAwesome}
                        onChange={this.handleChange}
                    />
                    <br />
                    <br />
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
}
