import React, { Component } from 'react';

export default class Checkbox extends Component {
    state = {
        isAwesome: false,
    };

    handleChange = (e) => {
        // if (e.target.type === 'checkbox') {
        //     this.setState({
        //         isAwesome: e.target.checked,
        //     });
        // }

        if (e.target.name === 'checkboxField') {
            this.setState({
                isAwesome: e.target.checked,
            });
        }
    };

    render() {
        const { isAwesome } = this.state;
        return (
            <div>
                <form>
                    <p>Reasult: {isAwesome === true ? 'Checked' : 'Unchecked'}</p>
                    <input
                        type="checkbox"
                        name="checkboxField"
                        checked={isAwesome}
                        onChange={this.handleChange}
                    />
                </form>
            </div>
        );
    }
}
