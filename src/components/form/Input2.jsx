import React, { Component } from 'react';

export default class Input extends Component {
    state = {
        title: '',
        placeholder: 'Enter title',
    };

    handleChange = (e) => {
        // if (e.target.type === 'text') {
        //     this.setState({
        //         title: e.target.value,
        //     });
        // }
        if (e.target.name === 'inputField') {
            this.setState({
                title: e.target.value,
            });
        }
    };

    render() {
        const { title, placeholder } = this.state;
        return (
            <div>
                <form>
                    <p>Reasult: {title}</p>
                    <input
                        type="text"
                        name="inputField"
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
                </form>
            </div>
        );
    }
}
