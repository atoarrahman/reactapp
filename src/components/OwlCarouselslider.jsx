import React, { Component } from 'react';
import OwlCarousel from 'react-owl-carousel3';
import slide1 from '../asset/images/slider1.jpg';
import slide2 from '../asset/images/slider2.jpg';
import slide3 from '../asset/images/slider3.jpg';

export default class OwlCarouselslider extends Component {
    render() {
        return (
            <OwlCarousel className="owl-theme" items={2} autoplay loop dots nav margin={10}>
                <div className="item">
                    <img src={slide1} alt="" srcSet="" />
                </div>
                <div className="item">
                    <img src={slide2} alt="" srcSet="" />
                </div>
                <div className="item">
                    <img src={slide3} alt="" srcSet="" />
                </div>
            </OwlCarousel>
        );
    }
}
