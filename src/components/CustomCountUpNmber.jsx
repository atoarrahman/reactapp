import React, { useCallback, useEffect, useRef, useState } from 'react';

export default function CustomCountUpNmber({ start = 0, end = 200, timer = 100 }) {
    const [count, setCount] = useState(null);
    const [elementPosition, setElementPosition] = useState(0);
    const [scrollPosition, setScrollPosition] = useState(0);

    // useRef as a Storage
    const ref = useRef(start);

    // Call Count Element Refarence
    const countRef = useRef(null);

    // Set Count Data
    const updateCounterState = useCallback(() => {
        if (ref.current < end) {
            const result = Math.ceil(ref.current + 1);
            if (result > end) return setCount(end);

            setCount(result);
            ref.current = result;
        }
        return setTimeout(updateCounterState, timer);
    }, [end, timer]);

    useEffect(() => {
        // Set the Element Position
        setElementPosition(countRef.current.offsetTop - window.innerHeight);
        // Set the Scroll Position
        const updatePosition = () => {
            setScrollPosition(window.pageYOffset);
        };
        // On Scroll Function Call
        window.addEventListener('scroll', updatePosition);
        updatePosition();
        if (scrollPosition > elementPosition) {
            updateCounterState();
        }
        return () => window.removeEventListener('scroll', updatePosition);
    }, [scrollPosition, elementPosition, updateCounterState]);

    return <h1 ref={countRef}>{count}</h1>;
}
