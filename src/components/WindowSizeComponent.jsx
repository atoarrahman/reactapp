import useWindowSize from '../hooks/useWindowSize';

export default function WindowSizeComponent() {
    const windowSize = useWindowSize();

    return (
        <div>
            <h1>
                You are browsing on window width: {windowSize.width} and height: {windowSize.height}{' '}
                device
            </h1>
        </div>
    );
}
